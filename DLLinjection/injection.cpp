#pragma once

#include "stdafx.h"
#include "injection.h"

bool ManualMap(HANDLE hProc, const char *szDllFile) {
	HANDLE hDLLfile = NULL;
	HANDLE hThread = NULL;
	BYTE *pSrcData = 0;
	BYTE *pTargetBase = 0;
	BYTE *pSrcBase = 0;
	void *pShellcode = 0;
	bool ret = false;

	__try {
		// opening DLL
		printf("Opening DLL file...\n");
		hDLLfile = CreateFileA(szDllFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

		if (hDLLfile == INVALID_HANDLE_VALUE) {
			printf("Failed to open DLL file [%d]\n", GetLastError());
			__leave;
		}

		DWORD DLLfileLength = GetFileSize(hDLLfile, NULL);

		if (DLLfileLength == INVALID_FILE_SIZE) {
			printf("Invalid DLL file length [%d]\n", GetLastError());
			__leave;
		}

		// load DLL into memory
		printf("Allocating memory for DLL file...\n");
		pSrcData = (BYTE *)malloc(DLLfileLength);

		if (pSrcData == nullptr) {
			printf("Failed to allocate memory for DLL\n");
			__leave;
		}

		DWORD bytesRead;
		printf("Loading DLL file into memory...\n");
		if (!ReadFile(hDLLfile, pSrcData, DLLfileLength, &bytesRead, NULL)) {
			printf("Failed to read file [%d]\n", GetLastError());
			__leave;
		}

		// DLL verification checks
		printf("DLL verification checks...\n");
		IMAGE_DOS_HEADER *doshd = (IMAGE_DOS_HEADER *)pSrcData;
		if (doshd->e_magic != IMAGE_DOS_SIGNATURE) {
			printf("Not a valid DOS header\n");
			__leave;
		}
		IMAGE_NT_HEADERS *pOldNtHeader = (IMAGE_NT_HEADERS *)(pSrcData + doshd->e_lfanew);
		IMAGE_OPTIONAL_HEADER *pOldOptHeader = &pOldNtHeader->OptionalHeader;
		IMAGE_FILE_HEADER *pOldFileHeader = &pOldNtHeader->FileHeader;

		if (pOldNtHeader->Signature != IMAGE_NT_SIGNATURE) {
			printf("Not a valid PE header\n");
			__leave;
		}

		if (!(pOldNtHeader->FileHeader.Characteristics & IMAGE_FILE_DLL)) {
			printf("Not a valid DLL file\n");
			__leave;
		}

#ifdef _WIN64
		if (pOldFileHeader->Machine != IMAGE_FILE_MACHINE_AMD64) {
			printf("Invalid platform\n");
			__leave;
		}
#else
		if (pOldFileHeader->Machine != IMAGE_FILE_MACHINE_I386) {
			printf("Invalid platform\n");
			__leave;
		}
#endif

		// allocating remote memory
		printf("Allocating memory in target process...\n");
		pTargetBase = (BYTE *)VirtualAllocEx(hProc, (void *)pOldNtHeader->OptionalHeader.ImageBase, pOldNtHeader->OptionalHeader.SizeOfImage, MEM_RESERVE | MEM_COMMIT, PAGE_EXECUTE_READWRITE);

		if (!pTargetBase) {
			pTargetBase = (BYTE *)VirtualAllocEx(hProc, NULL, pOldNtHeader->OptionalHeader.SizeOfImage, MEM_RESERVE | MEM_COMMIT, PAGE_EXECUTE_READWRITE);
			if (!pTargetBase) {
				printf("Failed to allocate memory in target process [%d]\n", GetLastError());
				__leave;
			}
		}

		// allocate local memory
		pSrcBase = (BYTE *)malloc(pOldNtHeader->OptionalHeader.SizeOfImage);

		if (pSrcBase == nullptr) {
			printf("Failed to allocate local memory for DLL headers\n");
			VirtualFreeEx(hProc, pTargetBase, 0, MEM_RELEASE);
			__leave;
		}

		// write DLL headers to local memory
		memcpy(pSrcBase, pSrcData, pOldNtHeader->OptionalHeader.SizeOfHeaders);
		IMAGE_SECTION_HEADER *pSectionHeader = IMAGE_FIRST_SECTION(pOldNtHeader);
		for (int i = 0; i < pOldNtHeader->FileHeader.NumberOfSections; i++, pSectionHeader++)
			memcpy(pSrcBase + pSectionHeader->VirtualAddress, pSrcData + pSectionHeader->PointerToRawData, pSectionHeader->SizeOfRawData);

		// base relocation
		printf("Performing base relocation...\n");
		baseRelocation(pTargetBase, pSrcBase);

		// write DLL to process
		printf("Copying headers to target process at %p...\n", pSrcData);
		if (!WriteProcessMemory(hProc, pTargetBase, pSrcBase, pOldNtHeader->OptionalHeader.SizeOfImage, NULL)) {
			printf("Unable to map sections [%d]\n", GetLastError());
			VirtualFreeEx(hProc, pTargetBase, 0, MEM_RELEASE);
			__leave;
		}

		printf("Allocating memory in target process for shellcode...\n");
		pShellcode = VirtualAllocEx(hProc, nullptr, 0x1000, MEM_RESERVE | MEM_COMMIT, PAGE_EXECUTE_READWRITE);
		if (!pShellcode) {
			printf("Failed to allocate memory in target process [%d]\n", GetLastError());
			VirtualFreeEx(hProc, pTargetBase, 0, MEM_RELEASE);
			__leave;
		}

		// manual map data
		MANUAL_MAPPING_DATA data{ 0 };
		data.pLoadLibraryA = LoadLibraryA;
		data.pGetProcAddress = GetProcAddress;
		WriteProcessMemory(hProc, pTargetBase, &data, sizeof(data), NULL);

		printf("Writing shell code to target process...\n");
		WriteProcessMemory(hProc, pShellcode, Shellcode, 0x1000, NULL);

		printf("Creating remote thread...\n");
		hThread = CreateRemoteThread(hProc, NULL, 0, (LPTHREAD_START_ROUTINE)pShellcode, pTargetBase, 0, NULL);
		if (!hThread) {
			printf("Failed to create thread [%d]\n", GetLastError());
			VirtualFreeEx(hProc, pTargetBase, 0, MEM_RELEASE);
			__leave;
		}

		WaitForSingleObject(hThread, INFINITE);

		printf("Manual injection completed...\n");
		ret = true;
	} __finally {
		if (hDLLfile)
			CloseHandle(hDLLfile);
		if (pSrcData)
			free(pSrcData);
		if (pSrcBase)
			free(pSrcBase);
		if (pShellcode)
			VirtualFreeEx(hProc, pShellcode, 0, MEM_RELEASE);
		if (hThread)
			CloseHandle(hThread);
	}

	return ret;
}

void baseRelocation(BYTE *targetBase, BYTE *srcBase) {
	IMAGE_OPTIONAL_HEADER * pOpt = &((IMAGE_NT_HEADERS *)(srcBase + ((IMAGE_DOS_HEADER *)srcBase)->e_lfanew))->OptionalHeader;
	BYTE *delta = targetBase - pOpt->ImageBase;

	if (delta) {
		IMAGE_BASE_RELOCATION *pRelocData = (IMAGE_BASE_RELOCATION *)(srcBase + pOpt->DataDirectory[IMAGE_DIRECTORY_ENTRY_BASERELOC].VirtualAddress);

		while (pRelocData->VirtualAddress) {
			DWORD AmountOfEntries = (pRelocData->SizeOfBlock - sizeof(IMAGE_BASE_RELOCATION)) / sizeof(WORD);
			WORD *pRelativeInfo = (WORD *)(pRelocData + 1);

			for (DWORD i = 0; i < AmountOfEntries; i++) {
				if (RELOC_FLAG(*pRelativeInfo)) {
					WORD offset = pRelativeInfo[i] & 0xFFF;
					UINT_PTR *pPatch = (UINT_PTR *)(srcBase + pRelocData->VirtualAddress + offset);
					*pPatch += (UINT_PTR)delta;
				}
			}

			pRelocData = (IMAGE_BASE_RELOCATION *)((BYTE *)pRelocData + pRelocData->SizeOfBlock);
		}
	}
}

void __stdcall Shellcode(MANUAL_MAPPING_DATA *pData) {
	if (!pData)
		return;

	BYTE *pBase = (BYTE *) pData;
	IMAGE_OPTIONAL_HEADER * pOpt = &((IMAGE_NT_HEADERS *) (pBase + ((IMAGE_DOS_HEADER *) pData)->e_lfanew))->OptionalHeader;

	auto _LoadLibraryA = pData->pLoadLibraryA;
	auto _GetProcAddress = pData->pGetProcAddress;
	auto _DllMain = (f_DLL_ENTRY_POINT)(pBase + pOpt->AddressOfEntryPoint);

	// imports
	if (pOpt->DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].Size) {
		IMAGE_IMPORT_DESCRIPTOR *pImportDescr = (IMAGE_IMPORT_DESCRIPTOR *) (pBase + pOpt->DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].VirtualAddress);
		while (pImportDescr->Name) {
			char * szMod = (char *) (pBase + pImportDescr->Name);
			HINSTANCE hDll = _LoadLibraryA(szMod);
			ULONG_PTR *pThunkRef = (ULONG_PTR *) (pBase + pImportDescr->OriginalFirstThunk);
			ULONG_PTR *pFuncRef = (ULONG_PTR *) (pBase + pImportDescr->FirstThunk);

			if (!pThunkRef)
				pThunkRef = pFuncRef;

			for (; *pThunkRef; ++pThunkRef, ++pFuncRef) {
				if (IMAGE_SNAP_BY_ORDINAL(*pThunkRef)) {
					*pFuncRef = (ULONG_PTR) _GetProcAddress(hDll, (char *) (*pThunkRef & 0xFFFF));
				} else {
					IMAGE_IMPORT_BY_NAME *pImport = (IMAGE_IMPORT_BY_NAME *) (pBase + (*pThunkRef));
					*pFuncRef = (ULONG_PTR) _GetProcAddress(hDll, pImport->Name);
				}
			}
			pImportDescr++;
		}
	}


	// TLS
	if (pOpt->DataDirectory[IMAGE_DIRECTORY_ENTRY_TLS].Size) {
		IMAGE_TLS_DIRECTORY *pTLS = (IMAGE_TLS_DIRECTORY *) (pBase + pOpt->DataDirectory[IMAGE_DIRECTORY_ENTRY_TLS].VirtualAddress);
		PIMAGE_TLS_CALLBACK* pCallback = (PIMAGE_TLS_CALLBACK *) (pTLS->AddressOfCallBacks);

		for (; pCallback && *pCallback; pCallback++)
			(*pCallback)(pBase, DLL_PROCESS_ATTACH, nullptr);
	}

	_DllMain((HMODULE) pBase, DLL_PROCESS_ATTACH, nullptr);

	pData->hMod = (HINSTANCE) pBase;
}