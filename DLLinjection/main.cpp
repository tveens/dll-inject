// DLLinjection.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "injection.h"

#define PATH_BUF_SIZE 512
#define METHOD 2

const char szDllFile[] = "C:\\Users\\Thomas\\source\\repos\\DLLinjection\\x64\\Release\\reflective_dll.x64.dll";
const char szProc[] = "cmd.exe";

// prints a list of names and pids of running processes
DWORD findProcId(void) {
	HANDLE hProcessSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);
	if (hProcessSnap == INVALID_HANDLE_VALUE) {
		printf("Failed to get snapshot [%d]\n", GetLastError());
		return NULL;
	}

	PROCESSENTRY32 pe32;
	pe32.dwSize = sizeof(PROCESSENTRY32);

	bool bRet = Process32First(hProcessSnap, &pe32);

	while (bRet) {
		if (!strcmp(szProc, pe32.szExeFile)) {
			CloseHandle(hProcessSnap);
			return pe32.th32ProcessID;
		}
		bRet = Process32Next(hProcessSnap, &pe32);
	}

	CloseHandle(hProcessSnap);
	return NULL;
}


// writes the full path of the dll in the process specified by the handle
// returns true when successfull
bool allocPathMem(HANDLE hProc, const char *dllPath, void **allocMemAddress) {
	*allocMemAddress = (void *) VirtualAllocEx(hProc, NULL, strlen(dllPath), MEM_RESERVE | MEM_COMMIT, PAGE_EXECUTE_READWRITE);
	if (*allocMemAddress == 0) {
		printf("Failed to allocate memory in process [%d]\n", GetLastError());
		return false;
	}

	printf("Writing into the current process space at %p\n", *allocMemAddress);
	if (!WriteProcessMemory(hProc, *allocMemAddress, dllPath, strlen(dllPath), NULL)) {
		printf("WriteProcessMemory Failed [%u]\n", GetLastError());
		return false;
	}

	return true;
}


// finds the memory address of LoadLibraryA in kernel32.dll
// returns true when succesfull
bool findLoadLibAddr(void **libAddress) {
	*libAddress = (void *) GetProcAddress(GetModuleHandleA("kernel32.dll"), "LoadLibraryA");

	if (*libAddress == NULL) {
		printf("Failed to find kernel32.dll module [%d]\n", GetLastError());
		return false;
	}
	printf("kernel32.dll module address at 0x%p\n", *libAddress);

	return true;
}

HMODULE GetRemoteModuleHandle(HANDLE proc, const char* module_name) {
	DWORD proc_id = GetProcessId(proc);
	HANDLE hProcSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, proc_id);
	MODULEENTRY32 modEntry;
	modEntry.dwSize = sizeof(MODULEENTRY32);
	Module32First(hProcSnapshot, &modEntry);   
	do {
		if (!_stricmp(modEntry.szModule, module_name))
			return modEntry.hModule;
		modEntry.dwSize = sizeof(MODULEENTRY32);
	} while (Module32Next(hProcSnapshot, &modEntry));

	return NULL;
}

DWORD_PTR GetRemoteProcAddress(HANDLE proc, const char *mod, const char *func) {
	HMODULE remoteMod = GetRemoteModuleHandle(proc, mod);
	HMODULE localMod = GetModuleHandleA(mod);

	DWORD_PTR delta = (DWORD_PTR) remoteMod - (DWORD_PTR) localMod;

	return (DWORD_PTR) GetProcAddress(localMod, func) + delta;
}

IMAGE_SECTION_HEADER* GetEnclosingSectionHeader(DWORD_PTR rva, IMAGE_NT_HEADERS& nt_header) {
	IMAGE_SECTION_HEADER* section = IMAGE_FIRST_SECTION(&nt_header);

	for (int i = 0; i < nt_header.FileHeader.NumberOfSections; i++, section++) {
		// This 3 line idiocy is because Watcom's linker actually sets the
		// Misc.VirtualSize field to 0.  (!!! - Retards....!!!)
		DWORD_PTR size = section->Misc.VirtualSize;
		if (size == 0)
			size = section->SizeOfRawData;

		// Is the RVA within this section?
		if (rva >= section->VirtualAddress && rva < (section->VirtualAddress + size))
			return section;
	}

	return 0;
}

BYTE* GetPtrFromRVA(DWORD_PTR rva, IMAGE_NT_HEADERS& nt_header, BYTE *imageBase) {
	IMAGE_SECTION_HEADER* section = GetEnclosingSectionHeader(rva, nt_header);
	if (!section)
		return 0;
	LONG_PTR delta = (LONG_PTR) (section->VirtualAddress - section->PointerToRawData);
	return imageBase + rva - delta;
}
/*
// manually maps the DLL in the memory of hProc
// returns true when successfull
REMOTEDLLENTRY *allocDLL(HANDLE hProc, const char *dllPath) {


	// copy headers
	printf("Writing headers into the target process space at 0x%p\n", rImageBase);
	if (!WriteProcessMemory(hProc, rImageBase, image, nthd->OptionalHeader.SizeOfHeaders, NULL)) {
		printf("WriteProcessMemory Failed [%u]\n", GetLastError());
		free(image);
		return NULL;
	}

	IMAGE_SECTION_HEADER *secHead = IMAGE_FIRST_SECTION(nthd);
	for (int i = 0; i < nthd->FileHeader.NumberOfSections; i++, secHead++)
		WriteProcessMemory(hProc, (LPVOID) ((DWORD_PTR) ImageBase + secHead->VirtualAddress), (LPCVOID) ((DWORD_PTR) image + secHead->PointerToRawData), secHead->SizeOfRawData, NULL);

	BYTE *localImageBase = (BYTE *) malloc(nthd->OptionalHeader.ImageBase);
	memcpy(localImageBase, image, nthd->OptionalHeader.SizeOfHeaders);
	for (int i = 0; i < nthd->FileHeader.NumberOfSections; i++, secHead++)
		memcpy(localImageBase + secHead->VirtualAddress, image + secHead->PointerToRawData, secHead->SizeOfRawData);
	image = localImageBase;



	
	// base relocation
	IMAGE_BASE_RELOCATION *BaseRelocation = (IMAGE_BASE_RELOCATION *) (image + nthd->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_BASERELOC].VirtualAddress);
	DWORD BaseRelocationSize = nthd->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_BASERELOC].Size;
	DWORD_PTR delta = (DWORD_PTR) (ImageBase - nthd->OptionalHeader.ImageBase);

	if (delta != 0) {
		for (long RemainingBytes = BaseRelocationSize; RemainingBytes > 0; RemainingBytes -= BaseRelocation->SizeOfBlock) {
			DWORD numRelocations = (BaseRelocation->SizeOfBlock - sizeof(IMAGE_BASE_RELOCATION)) / sizeof(WORD);
			WORD *relocs = (WORD *) (BaseRelocation + 1);

			for (DWORD i = 0; i < numRelocations; i++) {
				WORD offset = relocs[i] & 0xFFF;
				BYTE type = relocs[i] >> 12;

				if (type == IMAGE_REL_BASED_HIGHLOW) {
					DWORD *ptr = (DWORD *) (image + BaseRelocation->VirtualAddress + offset);
					*ptr += delta;
				}
			}

			BaseRelocation = (IMAGE_BASE_RELOCATION *) ((BYTE *) BaseRelocation + BaseRelocation->SizeOfBlock);
		}
	}

	// resolve imports
	IMAGE_IMPORT_DESCRIPTOR *importDescriptor = (IMAGE_IMPORT_DESCRIPTOR *) image + nthd->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].VirtualAddress;
	DWORD importsDirSize = nthd->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].Size;
	
	if (importsDirSize) {
		for (; importDescriptor->OriginalFirstThunk; importDescriptor++) {
			HMODULE localModule = LoadLibraryA((LPCSTR) importDescriptor->Name);
			if (!GetRemoteModuleHandle(hProc, (const char*) importDescriptor->Name))
				allocDLL(hProc, (const char*) importDescriptor->Name);

			IMAGE_THUNK_DATA *OriginalFirstThunk = (IMAGE_THUNK_DATA *) (image + importDescriptor->OriginalFirstThunk);
			IMAGE_THUNK_DATA *FirstThunk = (IMAGE_THUNK_DATA *) (image + importDescriptor->FirstThunk);

			for (; OriginalFirstThunk->u1.AddressOfData; OriginalFirstThunk++, FirstThunk++) {
				if (OriginalFirstThunk->u1.Ordinal & IMAGE_ORDINAL_FLAG) {
					IMAGE_IMPORT_BY_NAME * iibn = (IMAGE_IMPORT_BY_NAME *) OriginalFirstThunk->u1.AddressOfData;
					FirstThunk->u1.Function = GetRemoteProcAddress(hProc, (const char*) importDescriptor->Name, iibn->Name);
				}
			}
		}
	}

	printf("DLL injected at 0x%p", ImageBase);
	free(image);

	REMOTEDLLENTRY *result = (REMOTEDLLENTRY *) malloc(sizeof(REMOTEDLLENTRY));
	result->ModuleBase = (DWORD_PTR) ImageBase;
	result->EntryPoint = (DWORD_PTR) (ImageBase + nthd->OptionalHeader.AddressOfEntryPoint);

	return result;
}

BOOL __stdcall DllMainWrapper(struct DLLMAINCALL *parameter) {
	return parameter->fpDllMain(parameter->hModule, parameter->ul_reason_for_call, parameter->lpReserved);
}
void DllMainWrapper_end(void) {
}

bool RemoteDllMainCall(HANDLE hProcess, LPVOID moduleEntry, HMODULE hModule, DWORD ul_reason_for_call, LPVOID lpReserved) {
	struct DLLMAINCALL dllMainCall = {(DLLMAIN) moduleEntry, hModule, ul_reason_for_call, lpReserved};
	SIZE_T DllMainWrapperSize = (SIZE_T) DllMainWrapper_end - (SIZE_T) DllMainWrapper;

	LPVOID lpParam = VirtualAllocEx(hProcess, NULL, sizeof(DLLMAINCALL), MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);
	LPVOID lpDllCallWrapper = VirtualAllocEx(hProcess, NULL, (SIZE_T) ( (DWORD_PTR)DllMainWrapper_end - (DWORD_PTR) DllMainWrapper), MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);

	if (!lpParam || !lpDllCallWrapper) {
		printf("Could not allocate memory in target process!");
		return false;
	}
	SIZE_T BytesWritten = 0;
	BOOL w1 = WriteProcessMemory(hProcess, lpParam, (LPCVOID) &dllMainCall, sizeof(struct DLLMAINCALL), &BytesWritten);
	BOOL w2 = WriteProcessMemory(hProcess, lpDllCallWrapper, (LPCVOID) DllMainWrapper, DllMainWrapperSize, &BytesWritten);

	if (!w1 || !w2) {
		printf("Could not write to memory in target process!");
		return false;
	}

	if (!FlushInstructionCache(hProcess, lpDllCallWrapper, DllMainWrapperSize)) {
		printf("Unable to flush instruction cache");
		return false;
	}

	DWORD dwThreadId = 0;
	HANDLE hThread = CreateRemoteThread(hProcess, NULL, NULL, (LPTHREAD_START_ROUTINE) lpDllCallWrapper, lpParam, NULL, &dwThreadId);
	if (!hThread) {
		printf("Could not create thread in remote process!");
		return false;
	}		
	
	if (!SetThreadPriority(hThread, THREAD_PRIORITY_TIME_CRITICAL)) {
		printf("Could not set thread priority.");
		return false;
	}

	if (WaitForSingleObject(hThread, 5000) == WAIT_FAILED) {
		printf("WaitForSingleObject failed.");
		return false;
	}

	DWORD dwExitCode = 0;
	if (!GetExitCodeThread(hThread, &dwExitCode)) {
		printf("Could not get thread exit code.");
		return false;
	}


	printf("Remote thread created!\n");

	return true;
}
*/

int main(int argc, char *argv[]) {
	DWORD pID = findProcId();
	if (pID == NULL) {
		system("PAUSE");
		return 1;
	}

	// Attach to the process specified by PID
	HANDLE hAttachProc = OpenProcess(PROCESS_CREATE_THREAD | PROCESS_QUERY_INFORMATION | PROCESS_VM_OPERATION | PROCESS_VM_READ | PROCESS_VM_WRITE, NULL, pID);
	if (hAttachProc == 0) {
		printf("Could not attach to process [%d]!\n", GetLastError());
		system("PAUSE");
		return 1;
	}

	if (METHOD == 1) {
		void *allocMemAddress = 0;
		if (!allocPathMem(hAttachProc, szDllFile, &allocMemAddress))
			exit(1);

		void *libAddress = 0;
		if (!findLoadLibAddr(&libAddress))
			exit(1);


		HANDLE rThread = CreateRemoteThread(hAttachProc, NULL, NULL, (LPTHREAD_START_ROUTINE) libAddress, allocMemAddress, NULL, NULL);

		if (rThread == NULL) {
			printf("Failed to create remote thread [%d]\n", GetLastError());
			CloseHandle(hAttachProc);
			system("PAUSE");
			return 1;
		}

		printf("Remote thread created!\n");
	} else if (METHOD == 2) {
		if (!ManualMap(hAttachProc, szDllFile)) {
			printf("Something went wrong...\n");
			CloseHandle(hAttachProc);
			system("PAUSE");
			return 1;
		}
	}

	CloseHandle(hAttachProc);
	system("PAUSE");
    return 0;
}

