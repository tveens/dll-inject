#pragma once

#include "stdafx.h"

#define RELOC_FLAG32(RelInfo) ((RelInfo >> 12) == IMAGE_REL_BASED_HIGHLOW)
#define RELOC_FLAG64(RelInfo) ((RelInfo >> 12) == IMAGE_REL_BASED_DIR64)
#ifdef _WIN64
#define RELOC_FLAG RELOC_FLAG64
#else
#define RELOC_FLAG RELOC_FLAG32
#endif

using f_LoadLibraryA = HINSTANCE(WINAPI*)(const char * lpLibFilename);
using f_GetProcAddress = FARPROC(WINAPI*)(HINSTANCE hModule, const char *lpProcName);
using f_DLL_ENTRY_POINT = BOOL(WINAPI*)(HMODULE hModule, DWORD ul_reason_for_call, void  *lpReserved);

struct MANUAL_MAPPING_DATA {
	f_LoadLibraryA pLoadLibraryA;
	f_GetProcAddress pGetProcAddress;
	HINSTANCE hMod;
};

bool ManualMap(HANDLE hProc, const char *szDllFile);
void baseRelocation(BYTE *targetBase, BYTE *srcBase);
void __stdcall Shellcode(MANUAL_MAPPING_DATA *pData);